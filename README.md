## Installation

- Clone this project.
- Open project folder on terminal.
- Run "cp .env.example .env"
- Run "composer install".
- Run "php artisan key:generate".
- This project use Postgre Sql but if you want to use mysql you must change database driver on .env
- Run "php artisan migrate"
- Run "php artisan db:seed" for database seeding.
- You must see the project in http://mobillium.test
 

## For using OAUTH2.0 API

- Run "php artisan passport:install" and save second client_id and client_secret
- Post http://mobillium.test/oauth/token with
```
curl --request POST \
  --url http://mobillium.test/oauth/token \
  --header 'Content-Type: multipart/form-data; boundary=---011000010111000001101001' \
  --form username=[ENTER YOUR USER EMAIL] \
  --form password=[ENTER YOUR USER PASSWORD] \
  --form grant_type=password \
  --form client_id=[ENTER YOUR CLIENT_ID] \
  --form client_secret=[ENTER YOUR CLIENT_SECRET]
```
- This request must return access token and you can use for Bearer Token.
- For testing api you must use http://mobillium.test/api/articles
    - GET api/articles returns article list.
    - GET api/articles/id return article with the given id
    - POST api/articles add article and return row
    ```
    curl --request POST \
      --url http://mobillium.test/api/articles \
      --header 'Authorization: Bearer [ENTER YOUR TOKEN]' \
      --header 'Content-Type: application/json' \
      --data '{
        "title": "eveniet test tests",
        "spot": "iste",
        "content": "Deserunt ipsam dolores quisquam consectetur. Animi est deserunt saepe aut voluptas.",
        "publishing_date": "2009-01-28"
    }'
    ``` 
    - PUT api/articles update the article and return message
     ```
        curl --request PUT \
          --url http://mobillium.test/api/articles \
          --header 'Authorization: Bearer [ENTER YOUR TOKEN]' \
          --header 'Content-Type: application/json' \
          --data '{
            "id":30,
            "title": "eveniet test tests",
            "spot": "iste",
            "content": "Deserunt ipsam dolores quisquam consectetur. Animi est deserunt saepe aut voluptas.",
            "publishing_date": "2009-01-28"
        }'
     ``` 
    - PUT api/article/id unpublish the article and return message
    - DELETE api/article/id delete the article and return message
## Testing
- For browser test
    - RUN "php artisan dusk:install"
    - RUN "php artisan dusk:chrome-driver --detect"
    - RUN "php artisan dusk" and wait for testing.
- For unit test
    - RUN "php artisan test"


<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    function getArticles()
    {
        try {
            return response()->json(['data' => Article::all(), 'message' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['data' => null, 'message' => $exception->getMessage()], 400);

        }
    }

    function getArticle($id)
    {
        try {
            return response()->json(['data' => Article::find($id), 'message' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['data' => null, 'message' => $exception->getMessage()], 400);

        }
    }
    function saveArticle(ArticleRequest $request)
    {
        try {
            $result = Article::createArticle($request);
            return response()->json(['data' => $result->toArray(), 'message' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['data' => null, 'message' => $exception->getMessage()], 400);

        }
    }
    function editArticle(ArticleRequest $request)
    {
        try {
            $result = Article::updateArticle($request);
            return response()->json(['data' => $result->toArray(), 'message' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['data' => null, 'message' => $exception->getMessage()], 400);

        }
    }
    function unPublishArticle($id)
    {
        try {
            $result = Article::unpublishArticle($id);
            $status = $result ? 'success':'failed';
            return response()->json(['data' => null, 'message' => $status], 200);
        } catch (\Exception $exception) {
            return response()->json(['data' => null, 'message' => $exception->getMessage()], 400);

        }
    }
    function deleteArticle($id)
    {
        try {
            $result = Article::removeArticle($id);
            $status = $result ? 'success':'failed';
            return response()->json(['data' => null, 'message' => $status], 200);
        } catch (\Exception $exception) {
            return response()->json(['data' => null, 'message' => $exception->getMessage()], 400);

        }
    }
}

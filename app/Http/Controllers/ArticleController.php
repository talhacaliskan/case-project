<?php

namespace App\Http\Controllers;

use App\Http\Services\VoteService;
use App\Models\Article;
use App\Models\Vote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    function article($slug){
        $service = new VoteService();
        $article = Article::getArticleBySlug($slug);
        $votingData = $service->prepareData($article->id);
        $votingResult = $service->calculateRate($votingData['last30'],$votingData['first70']);
        $user_vote = 0;
        if (Auth::user() !== null){
            $user_vote = Vote::getUserArticleVote(Auth::id(),$article->id);
        }
        return view('article')->with('article',$article)->with('voting_result',$votingResult)->with('user_vote',$user_vote);
    }

}

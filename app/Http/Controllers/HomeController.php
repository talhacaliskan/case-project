<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    function index()
    {
        $articles = Article::getHomePageArticles();
        return view('home')->with('articles', $articles);
    }

    function logout()
    {
        Auth::logout();
        return redirect('/');
    }

}

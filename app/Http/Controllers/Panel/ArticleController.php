<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Models\Article;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ArticleController extends Controller
{
    function index()
    {
        $user = Auth::user() !== null ? Auth::user() : null;
        $articles = Article::getArticles($user);
        return view('panel.index')->with('articles', $articles);
    }

    function create()
    {
        return view('panel.new');
    }

    function store(ArticleRequest $request)
    {
        $result = Article::createArticle($request);
        $redirect_url = isset($result->slug) ? '/article/' . $result->slug : '/panel';
        return redirect($redirect_url);
    }

    function edit($id)
    {
        $article = Article::getArticleById($id);
        return view('panel.edit')->with('article', $article);
    }

    function update(ArticleRequest $request)
    {
        $article = Article::updateArticle($request);
        return redirect('/panel');
    }

    function delete(Request $request)
    {
        Article::removeArticle($request->input('articleId'));
        //todo message
        return redirect('/panel');

    }

    function publish(Request $request)
    {
        Article::publishArticle($request->input('articleId'));
        return redirect('/panel');
    }

    function unpublish(Request $request)
    {
        Article::unpublishArticle($request->input('articleId'));
        return redirect('/panel');
    }
}

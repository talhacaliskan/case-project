<?php

namespace App\Http\Controllers;

use App\Http\Services\VoteService;
use App\Models\Article;
use App\Models\User;
use App\Models\Vote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VoteController extends Controller
{
    function vote(Request $request){
        $result = Vote::sendVote($request);
        return redirect($request->session()->previousUrl());
    }

}

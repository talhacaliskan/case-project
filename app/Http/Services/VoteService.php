<?php


namespace App\Http\Services;


use App\Models\Article;

class VoteService
{
    function prepareData($articleId){
        $count = Article::voteCount($articleId);
        $last30PercentageCount = $this->calculatePercentageCount($count, 30);
        $first70PercentageCount = $count - $last30PercentageCount;
        $last30PercentVotes = Article::getLastNVotes($articleId, $last30PercentageCount);
        $first70PercentageVotes = Article::getFirstNVotes($articleId, $first70PercentageCount);
        return ['last30'=>$last30PercentVotes,'first70'=>$first70PercentageVotes];
    }
    function calculateRate($last30,$first70)
    {
        $totalVoteValue1 = $this->calculateVoteRate($last30);
        $totalVoteValue2 = $this->calculateVoteRate($first70);
        return 2 * $totalVoteValue1 + $totalVoteValue2;
    }

    protected function calculateVoteRate($votes)
    {
        $result = 0;
        foreach ($votes as $vote) {
            $result += $vote['pivot']['value'];
        }
        return $result;
    }

    protected function calculatePercentageCount($totalCount, $percentage)
    {
        return (int)($percentage / 100) * $totalCount;
    }

}

<?php

namespace App\Models;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Whtht\PerfectlyCache\Traits\PerfectlyCachable;


class Article extends Model
{
    use SoftDeletes;
    use Sluggable;
    use HasFactory;
    use PerfectlyCachable;

    protected $fillable = ['title', 'slug', 'content'];

    //Business Area
    static function getArticleById($id)
    {
        return Article::find($id);
    }

    static function getArticleBySlug($slug)
    {
        return Article::where('slug', $slug)->first();
    }

    static function getArticlesByAuthor()
    {

    }

    static function getArticles($user)
    {
        if ($user->role == 'writer') {
            return Article::where('user_id', $user->id)->get();
        } else {
            return Article::all();
        }
    }
    static function getHomePageArticles(){
        return Article::where('publishing_date', '<=', Carbon::now())->get();
    }
    static function createArticle($request)
    {
        $article = new Article();
        $article->title = $request->title;
        $article->spot = $request->spot;
        $article->content = $request->content;
        $article->publishing_date = $request->date;
        $article->user_id = Auth::id();
        $article->save();
        return $article;
    }

    static function updateArticle($request)
    {
        $article = Article::find($request->input('id'));
        $article->title = $request->input('title');
        $article->spot = $request->input('spot');
        $article->content = $request->input('content');
        $article->publishing_date = $request->input('date');
        $article->user_id = Auth::id();
        $article->save();
        return $article;
    }

    static function removeArticle($id)
    {
        $article = Article::find($id);
        if (in_array(Auth::user()->role, ['admin', 'moderator']) || Auth::id() == $article->user_id) {
            $article->delete();
            return true;
        } else {
            return false;
        }
    }

    static function publishArticle($id)
    {
        $article = Article::find($id);
        if (!is_null($article)) {
            $article->publishing_date = Carbon::now();
            $article->save();
            return true;
        } else {
            return false;
        }
    }

    static function unpublishArticle($id)
    {
        $article = Article::find($id);
        if (!is_null($article)) {
            $article->publishing_date = null;
            $article->save();
            return true;
        } else {
            return false;
        }
    }

    static function voteCount($id)
    {
        return Article::find($id)->votedUsers()->get()->count();
    }

    static function getLastNVotes($id, $count)
    {
        return Article::find($id)->votedUsers()->orderBy('created_at', 'desc')->take($count)->get();
    }

    static function getFirstNVotes($id, $count)
    {
        return Article::find($id)->votedUsers()->orderBy('created_at', 'asc')->take($count)->get();
    }

    static function getRandomNArticle($count)
    {
        return DB::table('articles')
            ->inRandomOrder()
            ->where('deleted_at','=',null)
            ->take($count)->get();
    }

    //Relation
    function user()
    {
        return $this->belongsTo(User::class);
    }

    function votedUsers()
    {
        return $this->belongsToMany(User::class, 'votes')->withPivot('value');
    }

    //slugify package
    function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}

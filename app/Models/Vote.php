<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Support\Facades\Auth;

class Vote extends Pivot
{
    use HasFactory;
    protected $table = "votes";

    static function sendVote($request){
        $checkExit = Vote::where('user_id',Auth::id())->where('article_id',$request->articleId)->first();
        $vote = null;
        if ($checkExit == null){
            $vote = new Vote();
            $vote->user_id = Auth::id();
            $vote->article_id = $request->articleId;
            $vote->value = $request->vote;
            $vote->save();
        }
        else{
            Vote::where('user_id',Auth::id())->where('article_id',$request->articleId)->update(['value'=>$request->vote]);
        }
        return $vote;
    }
    static function getUserArticleVote($userId, $articleId)
    {
        $vote = Vote::where('user_id', $userId)->where('article_id', $articleId)->first();
        $value = 0;
        if ($vote != null) {
            $value = $vote->value;
        }
        return $value;
    }

}

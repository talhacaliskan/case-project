<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Mobillium',
                'email' => 'admin@mobillium.com',
                'password' => Hash::make('mobillium'),
                'role'=>'admin'
            ],
            [
                'name' => 'Mobillium',
                'email' => 'writer1@mobillium.com',
                'password' => Hash::make('mobillium'),
                'role'=>'writer'
            ]
        ]);

        //User::factory(10)->create();
        Article::factory(25)->create();
        DB::table('votes')->insert([
            'user_id'=>2,
            'article_id'=>1,
            'value'=>4
        ]);
    }
}

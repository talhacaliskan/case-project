<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Mobillium</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Styles -->


    <style>
        body {
            font-family: 'Nunito', sans-serif;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <script src="{{ asset('js/app.js') }}" defer></script>

</head>
<body style="background-color: #1a202c !important;" class="antialiased">
<div class="relative flex items-top justify-center min-h-screen  sm:items-center py-4 sm:pt-0">
    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
        <div class="flex justify-between pt-8 sm:justify-between sm:pt-0">
            <h3 class="dark:text-white text-white">{{Auth::user()->role == 'writer' ? 'Author' : 'Admin'}} Panel</h3>
            <a href="/panel"><h1 class="dark:text-white text-white">Posts</h1></a>
        </div>

        <div class="mt-8 justify-center  overflow-hidden ">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li class="text-white">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="/panel/update" method="POST">
                @csrf
                <input type="hidden" name="id" value="{{$article->id}}">
                <div class="justify-center flex flex-col">
                    <label class="text-white" for="">Title</label>
                    <input name="title" value="{{$article->title}}" class="mt-2 shadow appearance-none border rounded py-2 px-3 text-grey-darker">
                </div>
                <div class="justify-center flex mt-4 flex-col">
                    <label class="text-white" for="">Spot</label>

                    <textarea name="spot" class="mt-2 shadow appearance-none border rounded py-2 px-3 text-grey-darker"
                              cols="30" rows="2">{{$article->title}}</textarea>
                </div>
                <div class="justify-center flex mt-4 flex-col">
                    <label class="text-white" for="">Content</label>

                    <textarea name="content" class="mt-2 shadow appearance-none border rounded py-2 px-3 text-grey-darker"
                              cols="30" rows="5">{{$article->title}}</textarea>
                </div>
                <div class="justify-center flex mt-4 flex-col">
                    <label class="text-white" for="">Publishing Date</label>
                    <input name="date" type="date" value="{{$article->publishing_date}}" class="mt-2 shadow appearance-none border rounded py-2 px-3 text-grey-darker" />
                </div>
                <div class="justify-center flex mt-4 flex-col">
                    <input type="submit" value="Update" style="cursor: pointer !important;" class="mt-2 shadow appearance-none border rounded py-2 px-3 text-grey-darker" />
                </div>

            </form>


        </div>


    </div>
</div>
</body>
</html>

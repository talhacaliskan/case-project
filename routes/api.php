<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ArticleController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::middleware('auth:api')->get('/articles', [ArticleController::class, 'getArticles']);
Route::middleware('auth:api')->get('/articles/{id}', [ArticleController::class, 'getArticle']);
Route::middleware('auth:api')->post('/articles', [ArticleController::class, 'saveArticle']);
Route::middleware('auth:api')->put('/articles', [ArticleController::class, 'editArticle']);
Route::middleware('auth:api')->put('/articles/{id}', [ArticleController::class, 'unPublishArticle']);
Route::middleware('auth:api')->delete('/articles/{id}', [ArticleController::class, 'deleteArticle']);

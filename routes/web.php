<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\Panel\ArticleController as PanelArticleController;
use App\Http\Controllers\VoteController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class,'index']);
Route::get('/article/{slug}',[ArticleController::class,'article']);
Route::get('/logout',[HomeController::class,'logout']);
Route::get('/dashboard', [HomeController::class,'index'])->middleware(['auth'])->name('dashboard');
Route::post('/vote',[VoteController::class,'vote']);
Route::name('panel')
    ->prefix('panel')
    ->middleware(['auth','can:accessAdminPanel'])
    ->group(function (){
        Route::get('/',[PanelArticleController::class,'index']);
        Route::get('/new',[PanelArticleController::class,'create']);
        Route::post('/new',[PanelArticleController::class,'store']);
        Route::get('/edit/{id}',[PanelArticleController::class,'edit']);
        Route::post('/update',[PanelArticleController::class,'update']);
        Route::post('/delete',[PanelArticleController::class,'delete']);
        Route::post('/publish',[PanelArticleController::class,'publish']);
        Route::post('/unpublish',[PanelArticleController::class,'unpublish']);
    });
require __DIR__.'/auth.php';

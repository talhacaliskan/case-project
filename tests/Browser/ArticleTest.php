<?php

namespace Tests\Browser;

use App\Models\Article;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class ArticleTest extends DuskTestCase
{
    public function testMainPage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->assertSee('Mobillium');
        });
    }

    public function testRandomTenArticle()
    {
        $articles = Article::getRandomNArticle(10);
        foreach ($articles as $article) {
            $this->browse(function (Browser $browser) use ($article) {
                $browser->visit('/article/' . $article->slug)
                    ->assertSee($article->title);
            });
        }
    }
}

<?php

namespace Tests\Unit;

use App\Http\Services\VoteService;
use App\Models\Article;
use App\Models\Vote;
use Database\Factories\ArticleFactory;
use Database\Factories\VoteFactory;
use Tests\TestCase;

class CalculateTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testVoteRateCalculate()
    {
        $service = new VoteService();
        $last30service = Vote::factory()->count(30)->make();
        $first70service = Vote::factory()->count(70)->make();
        $result = $service->calculateRate($last30service, $first70service);
        $this->assertEquals(390, $result);
    }
}
